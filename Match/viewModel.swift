//
//  viewModel.swift
//  Match
//
//  Created by MALLOJJALA PAVAN TEJA on 3/12/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

class hNVModel {
    var hiddenNumsArray: [Int] = []
    private var hnm: hiddenNumbers
    
    init(hnm: hiddenNumbers ) {
        self.hnm = hnm
        self.hiddenNumsArray = hNVModel.addToArray(hnm: hnm)
    }
    
    private static func addToArray(hnm: hiddenNumbers) -> [Int] {

        let hiddenNumsArr = hnm.getNums()

        return hiddenNumsArr

    }
}
